DEVBOX_CONTAINER=cookiecutter_devenv_devbox

.PHONY: dev
dev: lint test

.PHONY: install_dev
install_dev:
	$(call CMD, pip install -r requirements_dev.txt)

.PHONY: lint
lint: install_dev
	$(call CMD, pycodestyle .)
	$(call CMD, pydocstyle hooks)

# test targets
test: unittest

.PHONY: unittest
unittest: install_dev
	$(call CMD, pytest --verbose tests)

-include dev/env.mk
