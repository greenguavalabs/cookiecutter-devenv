# cookiecutter-devenv

A template to add a development & ci environment to an existing project.

* Website: http://www.greenguavalabs.com/templates/cookiecutter-devenv/

## Features
* containerized development environment using [Docker](https://www.docker.com/)
 and [Docker Compose](https://www.docker.com/)
* `Makefile` customized for the particular programming language used
* `make` commands run inside Docker container unless `NO_DOCKER` environment variable is set
* support for multiple languages ([python](https://www.python.org/), [ruby](https://www.ruby-lang.org))
* [optional] integration with the [concourse](http://concourse.ci/) CI system

## Usage
Install `cookiecutter` command line:

```bash
pip install cookiecutter
```

Switch to a project folder and generate the `cookiecutter` template layout:

```bash
cookiecutter bb:greenguavalabs/cookiecutter-devenv.git
```

You will be asked for the information needed to customize the development
environment. Below is a sample of the prompts:

```bash
devenv_folder [dev]:
project_git_url [git@github.com:user/my-proj.git]: git@bitbucket.org:greenguavalabs/cookiecutter-devenv.git
project_slug [cookiecutter-devenv]:
Select programming_lang:
1 - python
2 - ruby
Choose from 1, 2 [1]:
programming_lang_version [latest]:
Select concourse_ci:
1 - yes
2 - no
Choose from 1, 2 [1]:
```

Launch the development environment Docker containers:

```bash
cd dev/docker
docker-compose up -d
```

Run the default target in the generated `Makefile` to verify the setup:

```bash
cd ../../
make
```

## License
This project is licensed under the terms of the [MIT License](/LICENSE)
