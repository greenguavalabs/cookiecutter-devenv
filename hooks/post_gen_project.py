# -*- coding: utf-8 -*-

"""Implements post-gen hooks for the cookiecutter-devenv template."""

import logging
import os
import shutil


def set_up_makefile():
    """Set up the language appropriate Makefile."""
    lang = "{{ cookiecutter.programming_lang }}"

    if lang is "python":
        dest = os.path.join("..", "Makefile")
        if os.path.exists(dest):
            shutil.move(dest, os.path.join("..", "Makefile.old"))

        shutil.move("Makefile-py", dest)
    else:
        os.remove("Makefile-py")
        os.remove("env.mk")


def set_up_concourse():
    """Set up the concourse ci config if appropriate."""
    enbled = "{{ cookiecutter.concourse_ci }}"

    if enbled is not "yes":
        shutil.rmtree("concourse")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(message)s')
    LOG = logging.getLogger('post_gen_project')

    set_up_makefile()
    set_up_concourse()
