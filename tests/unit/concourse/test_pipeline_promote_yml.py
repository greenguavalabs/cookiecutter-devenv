# -*- coding: utf-8 -*-

import pytest


class SetupPipelinePromoteYml:
    @pytest.fixture()
    def context(self):
        return {
            "project_slug": "my_proj",
            "project_git_url": "git@github.com:proj/my_proj.git",
            "devenv_folder": "foo"
        }

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        return template.project.join(
            "concourse", "pipeline_promote.yml").readlines()


class TestPipelinePromoteYml(SetupPipelinePromoteYml):
    def test_has_file(self, template):
        assert template.project.join(
            "concourse", "pipeline_promote.yml").isfile()

    def test_file_not_empty(self, file):
        assert len(file) is not 0

    testdata_lines = [
        "\
- name: my_proj-develop\n\
  type: git\n\
  source:\n\
    uri: git@github.com:proj/my_proj.git\n\
    branch: develop\n\
    private_key: {{bitbucket-private-key}}\n",

        "\
- name: my_proj-master\n\
  type: git\n\
  source:\n\
    uri: git@github.com:proj/my_proj.git\n\
    branch: master\n\
    private_key: {{bitbucket-private-key}}\n",

        "\
    - get: my_proj-develop\n\
      trigger: true\n\
    - task: verify\n\
      file: my_proj-develop/foo/concourse/tasks/verify.yml\n\
      input_mapping:\n\
        my_proj: my_proj-develop\n",

        "\
    - get: my_proj-develop\n\
      trigger: true\n\
    - task: unit-test\n\
      file: my_proj-develop/foo/concourse/tasks/unit_test.yml\n\
      input_mapping:\n\
        my_proj: my_proj-develop\n",

        "\
- name: git-push\n\
  plan:\n\
    - get: my_proj-develop\n",
        "\
    - put: my_proj-master\n\
      params:\n\
        repository: my_proj-develop\n"
    ]

    @pytest.mark.parametrize("line", testdata_lines)
    def test_contains_line(self, file, line):
        print(file)
        assert line in "".join(file)
