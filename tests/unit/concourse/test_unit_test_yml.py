# -*- coding: utf-8 -*-

import pytest


class SetupUnitTestYml:
    @pytest.fixture()
    def context(self):
        return {
            "programming_lang": "python",
            "programming_lang_version": "version1",
            "project_slug": "my-proj"
        }

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        return template.project.join(
            "concourse", "tasks", "unit_test.yml").readlines()


class TestUnitTestYml(SetupUnitTestYml):
    def test_has_file(self, template):
        assert template.project.join(
            "concourse", "tasks", "unit_test.yml").isfile()

    def test_file_not_empty(self, file):
        assert len(file) is not 0

    testdata_lines = [
        "    repository: python\n",
        "    tag: version1\n",
        "- name: my-proj\n",
        "    cd my-proj && NO_DOCKER=1 make unittest\n"
    ]

    @pytest.mark.parametrize("line", testdata_lines)
    def test_contains_line(self, file, line):
        assert line in file
