# -*- coding: utf-8 -*-

import pytest


class SetupDockerfile:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture
    def file(self, template):
        return template.project.join(
            "docker", "devbox", "Dockerfile").readlines()


class TestDockerfileDefault(SetupDockerfile):
    def test_has_devbox_docker_file(self, template):
        assert template.project.join(
            "docker", "devbox", "Dockerfile").isfile()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        ("FROM python:latest\n"),
        ("RUN pip install --upgrade pip\n"),
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestDockerfilePython(SetupDockerfile):
    testdata_prog_lang = [
        None,
        "foo",
        "python",
    ]

    @pytest.fixture(params=testdata_prog_lang)
    def context(self, request):
        return {
            "programming_lang": request.param,
            "programming_lang_version": "3.6"
        }

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        ("FROM python:3.6\n"),
        ("RUN pip install --upgrade pip\n"),
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestDockerfileRuby(SetupDockerfile):
    @pytest.fixture
    def context(self):
        return {
            "programming_lang": "ruby",
            "programming_lang_version": "2.2"
        }

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        ("FROM ruby:2.2\n"),
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
