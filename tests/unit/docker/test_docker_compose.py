# -*- coding: utf-8 -*-

import pytest


class SetupDockerCompose:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture
    def file(self, template):
        return template.project.join(
            "docker", "docker-compose.yml").readlines()


class TestDockerComposeDefault(SetupDockerCompose):
    def test_has_devbox_docker_compose(self, template):
        assert template.project.join(
            "docker", "docker-compose.yml").isfile()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "  my_proj_devbox:\n",
        "    image: my_proj:devbox\n",
        "    container_name: my_proj_devbox\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestDockerComposeCustom(SetupDockerCompose):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "cc_dev"}

    def test_has_devbox_docker_compose(self, template):
        assert template.project.join(
            "docker", "docker-compose.yml").isfile()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "  cc_dev_devbox:\n",
        "    image: cc_dev:devbox\n",
        "    container_name: cc_dev_devbox\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
