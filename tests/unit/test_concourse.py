# -*- coding: utf-8 -*-

import pytest


class SetupConcourse:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)


class TestConcourseDefault(SetupConcourse):
    def test_has_concourse_folder(self, template):
        assert template.project.join("concourse").isdir()


class TestConcourseExplicitEnabled(SetupConcourse):
    @pytest.fixture()
    def context(self):
        return {"concourse_ci": "yes"}

    def test_has_concourse_folder(self, template):
        assert template.project.join("concourse").isdir()


class TestConcourseDisabled(SetupConcourse):
    @pytest.fixture()
    def context(self):
        return {"concourse_ci": "no"}

    def test_does_not_have_concourse_folder(self, template):
        assert not template.project.join("concourse").exists()
