# -*- coding: utf-8 -*-

import pytest


class SetupMakefile:
    @pytest.fixture()
    def context(self):
        return {"devenv_folder": "foo"}

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        return template.project.join(
            "..", "Makefile").readlines()


class TestMakefileDefault(SetupMakefile):
    def test_has_makefile(self, template):
        assert template.project.join(
            "..", "Makefile").isfile()

    def test_does_not_have_makefile_py(self, template):
        assert not template.project.join(
            "Makefile-py").exists()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "DEVBOX_CONTAINER=my_proj_devbox\n",
        "-include foo/env.mk\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestMakefileNotPython(SetupMakefile):
    @pytest.fixture()
    def context(self):
        return {"programming_lang": "ruby"}

    testdata_files = [
        ("..", "Makefile"),
        ("Makefile-py"),
    ]

    @pytest.mark.parametrize("file", testdata_files)
    def test_does_not_have_file(self, template, file):
        assert not template.project.join(*file).exists()
