# -*- coding: utf-8 -*-

import pytest


class SetupTemplate:
    @pytest.fixture()
    def context(self):
        return {"devenv_folder": "foo"}

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)


class TestRoot(SetupTemplate):
    testdata_context = [
        None,
        {"devenv_folder": "foo"},
    ]

    @pytest.fixture(params=testdata_context)
    def context(self, request):
        return request.param

    def test_exit_code_is_zero(self, template):
        assert template.exit_code == 0

    def test_no_exception(self, template):
        assert template.exception is None

    def test_dir_created(self, template):
        assert template.project.isdir()

    def test_project_name_is_correct(self, template, context):
        expected = "dev" if context is None else context["devenv_folder"]
        assert template.project.basename == expected
